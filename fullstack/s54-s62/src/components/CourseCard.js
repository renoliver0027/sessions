import PropTypes from "prop-types";
import { useState } from "react";
import { Button, Card } from "react-bootstrap";

export default function CourseCard({ course }) {
  const { name, description, price } = course;

  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);

  function enroll() {
    setCount((prev_value) => prev_value + 1);

    if (count === 30) {
      return setCount(30);
    }
  }

  function availableSeats() {
    setSeats((prev_value) => prev_value - 1);

    if (seats === 0) {
      setSeats(0);
      return alert("No more seats available");
    }
  }

  return (
    <>
      <Card id="courseComponent1">
        <Card.Body>
          <Card.Title>
            <h5>{name}</h5>
          </Card.Title>

          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>

          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>

          <Card.Subtitle>Enrollees:</Card.Subtitle>
          <Card.Text>{count}</Card.Text>

          <Card.Subtitle>Available Seats:</Card.Subtitle>
          <Card.Text>{seats}</Card.Text>

          <Button
            variant="primary"
            onClick={() => {
              enroll();
              availableSeats();
            }}
          >
            Enroll
          </Button>
        </Card.Body>
      </Card>
    </>
  );
}

// PropTypes is used for validating the data from the props
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
