const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (request, response) => {
  const { name, email, password, isAdmin } = request.body;

  if (!name || !email || !password) {
    return response.send("Register failed! All fields are required.");
  }

  return User.findOne({ email })
    .then((user) => {
      if (user) {
        return response.send("User already registered, you may login instead.");
      } else {
        let new_user = new User({
          name,
          email,
          password: bcrypt.hashSync(password, 10),
          isAdmin,
        });

        return new_user.save().then((registered_user, error) => {
          if (error) {
            return response.send({ message: error.message });
          }
          return response.send({
            message: "Successfully registered a user!",
            user: registered_user,
          });
        });
      }
    })
    .catch((error) => response.send(error.message));
};

module.exports.loginUser = (request, response) => {
  const { email, password } = request.body;

  if (!email || !password) {
    return response.send("Login failed! Missing email or password.");
  }

  return User.findOne({ email })
    .then((user) => {
      if (!user) {
        return response.send("No user found! Please signup first.");
      }

      const isMatched = bcrypt.compareSync(password, user.password);

      if (isMatched) {
        return response.send({ accessToken: auth.createAccessToken(user) });
      } else {
        return response.send("Login failed! Incorrect Password.");
      }
    })
    .catch((error) => response.send(error.message));
};

module.exports.detailsUser = (request, response) => {
  const id = request.params.id;

  return User.findById(id)
    .select("-password")
    .then((user) => {
      if (!user) {
        return response.send("No user found! Please signup first.");
      }

      if (user.id.toString() !== request.user.id) {
        return response.send(
          "You don't have permission to retrieve this users' details."
        );
      }

      return response.send(user);
    })
    .catch((error) => response.send(error.message));
};

module.exports.allUsers = (request, response) => {
  return User.find({})
    .then((users) => {
      if (!users) {
        return response.send("No registered users");
      }
      return response.send(users);
    })
    .catch((error) => response.send(error.message));
};
