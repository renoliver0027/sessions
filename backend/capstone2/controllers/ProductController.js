const Product = require("../models/Product.js");
const mongoose = require("mongoose");

module.exports.createProduct = (request, response) => {
  const { name, description, price, isActive, createdOn } = request.body;

  if (!name || !description || !price) {
    return response
      .status(400)
      .send("Cannot register product! Missing name/description/price.");
  }

  return Product.findOne({ name }).then((product) => {
    if (product) {
      return response.status(409).send("Product already registered!");
    }

    let new_product = new Product({
      name,
      description,
      price,
      isActive,
      createdOn,
    });

    return new_product
      .save()
      .then((product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(201).send({
          message: "Product has been successfully registered!",
          product: product,
        });
      })
      .catch((error) => response.status(500).send(error.message));
  });
};

module.exports.getAllProducts = (request, response) => {
  return Product.find({})
    .then((products) => {
      if (!products) {
        return response.status(404).send("No products found :(");
      }

      return response.status(200).send(products);
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.getAllActiveProducts = (request, response) => {
  return Product.find({ isActive: true })
    .then((products) => {
      if (!products) {
        return response.status(404).send("No products found :(");
      }

      return response.status(200).send(products);
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.getSingleProduct = (request, response) => {
  if (!mongoose.isValidObjectId(request.params.id)) {
    return response.status(400).send("Error! Not a valid Mongo ID");
  }

  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send("Search failed! Product ID not registered.");
      }
      return response.status(200).send(product);
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.updateSingleProduct = (request, response) => {
  const { name, description, price } = request.body;

  if (!mongoose.isValidObjectId(request.params.id)) {
    return response.status(400).send("Error! Not a valid Mongo ID");
  }

  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send("Search failed! Product ID not registered.");
      }

      product.name = name;
      product.description = description;
      product.price = price;

      return product.save().then((updated_product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(updated_product);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.archiveProduct = (request, response) => {
  if (!mongoose.isValidObjectId(request.params.id)) {
    return response.status(400).send("Error! Not a valid Mongo ID");
  }

  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send("Search failed! Product ID not registered.");
      }

      product.isActive = false;

      return product.save().then((archived_product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(archived_product);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.activateProduct = (request, response) => {
  if (!mongoose.isValidObjectId(request.params.id)) {
    return response.status(400).send("Error! Not a valid Mongo ID");
  }

  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send("Search failed! Product ID not registered.");
      }

      product.isActive = true;

      return product.save().then((archived_product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(archived_product);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};
