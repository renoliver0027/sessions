const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const mongoose = require("mongoose");

module.exports.createOrder = async (request, response) => {
  const { productId, quantity } = request.body;

  if (!mongoose.isValidObjectId(productId)) {
    return response.status(400).send("Error! Not a valid Mongo ID");
  }

  let product_price = await Product.findById(productId).then((product) => {
    if (!product) {
      return response.status(404).send("No product registered.");
    }
    return product.price;
  });

  let new_order = new Order({
    userId: request.user.id,
    products: [{ productId, quantity }],
    totalAmount: product_price * quantity,
  });

  return await new_order
    .save()
    .then((newOrder, error) => {
      if (error) {
        return response.status(400).send(error.message);
      }
      return response.status(201).send(newOrder);
    })
    .catch((error) => response.status(500).send(error.message));
};
