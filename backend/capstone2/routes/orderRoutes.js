const express = require("express");
const OrderController = require("../controllers/OrderController.js");
const { verifyToken, verifyAdmin } = require("../auth.js");

const router = express.Router();

router.post("/checkout", verifyToken, (request, response) => {
  OrderController.createOrder(request, response);
});

module.exports = router;
