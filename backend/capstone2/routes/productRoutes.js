const express = require("express");
const ProductController = require("../controllers/ProductController.js");
const { verifyToken, verifyAdmin } = require("../auth.js");

const router = express.Router();

router.post("/create", verifyToken, verifyAdmin, (request, response) => {
  ProductController.createProduct(request, response);
});

router.get("/", verifyToken, (request, response) => {
  ProductController.getAllProducts(request, response);
});

router.get("/active", verifyToken, (request, response) => {
  ProductController.getAllActiveProducts(request, response);
});

router
  .route("/:id")
  .get(verifyToken, (request, response) => {
    ProductController.getSingleProduct(request, response);
  })
  .put(verifyToken, verifyAdmin, (request, response) => {
    ProductController.updateSingleProduct(request, response);
  });

router.put("/archive/:id", verifyToken, verifyAdmin, (request, response) => {
  ProductController.archiveProduct(request, response);
});

router.put("/activate/:id", verifyToken, verifyAdmin, (request, response) => {
  ProductController.activateProduct(request, response);
});

module.exports = router;
