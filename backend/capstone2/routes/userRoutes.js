const express = require("express");
const UserController = require("../controllers/UserController.js");
const { verifyToken, verifyAdmin } = require("../auth.js");

const router = express.Router();

router.post("/register", (request, response) => {
  UserController.registerUser(request, response);
});

router.post("/login", (request, response) => {
  UserController.loginUser(request, response);
});

router.get("/details/:id", verifyToken, (request, response) => {
  UserController.detailsUser(request, response);
});

router.get("/allUsers", verifyToken, verifyAdmin, (request, response) => {
  UserController.allUsers(request, response);
});

module.exports = router;
