// [SECTION] While Loop
// let count = 5;

// while (count !== 0) {
//   console.log("Current value of count: " + count);
//   count--;
// }

// [SECTION] Do-While Loop
// let number = Number(prompt("Give me a number:"));

// do {
//   console.log("Current value of number: " + number);

//   number += 1;
// } while (number < 10);

// [SECTION] For Loop
// for (let count = 0; count <= 20; count++) {
//   console.log("Current for loop value: " + count);
// }

// let my_string = "renz";

// console.log(my_string.length);

// console.log(my_string[2]);

// for (let i = 0; i < my_string.length; i++) {
//   console.log(my_string[i]);
// }

// MINI ACTIVITY
// 1. Loop through the 'my_name' variable which has a string with
// your name on it.
// 2. Display each letter in the console but exclude all the vowels from it.
// 3. Send a screenshot

// let my_name = "Renz Oliver Paguiligan";

// for (let i = 0; i < my_name.length; i++) {
//   //   const noVowels = my_name.replace(/[aeiou]/gi, "");
//   //   console.log(noVowels[i]);

//   if (
//     my_name[i].toLowerCase() == "a" ||
//     my_name[i].toLowerCase() == "e" ||
//     my_name[i].toLowerCase() == "i" ||
//     my_name[i].toLowerCase() == "o" ||
//     my_name[i].toLowerCase() == "u"
//   ) {
//     // If we use the 'continue' keyword, it will skip the else
//     // block and reiterate the loop to check if the next letter
//     // is a vowel
//     continue;
//   } else {
//     console.log(my_name[i]);
//   }
// }

let name_two = "oliver";

for (let i = 0; i < name_two.length; i++) {
  if (name_two[i].toLowerCase() == "o") {
    console.log("Skipping...");
    continue;
  }
  if (name_two[i].toLowerCase() == "e") {
    break;
  }
  console.log(name_two[i]);
}
