// [SECTION] If-Else statements

let number = 1;

if (number > 1) {
  console.log("The number is greater than 1!");
} else if (number < 1) {
  console.log("The number is less than 1!");
} else {
  console.log("None of the conditions were true :(");
}

// Falsy values
if (false) {
  console.log("Falsy");
}
if (0) {
  console.log("Falsy");
}
if (undefined) {
  console.log("Falsy");
}

// Truthy values
if (true) {
  console.log("Truthy");
}
if (1) {
  console.log("Truthy");
}
if ([]) {
  console.log("Truthy");
}

// Ternary Operators
let result = 1 < 10 ? true : false;

// This is the if-else equivalent of the ternary operation above.
// if (1 < 10) {
//   return true;
// } else {
//   return false;
// }

if (5 == 5) {
  let greeting = "hello";
  console.log(greeting);
}

console.log("Value returned from the ternary operator is " + result);

// [SECTION] Switch Statements
let day = prompt("What day of the week is it today?").toLowerCase();

switch (day) {
  case "monday":
    console.log("Today is monday!");
    break;
  case "tuesday":
    console.log("Today is tuesday!");
    break;
  case "wednesday":
    console.log("Today is wednesday!");
    break;
  case "thursday":
    console.log("Today is thursday!");
    break;
  case "friday":
    console.log("Today is friday!");
    break;
  case "saturday":
    console.log("Today is saturday!");
    break;
  case "sunday":
    console.log("Today is sunday!");
    break;
  default:
    console.log("Please input a valid day naman paareh");
    break;
}

// [SECTION] Try/Catch/Finally Statements

function showIntensityAlert(windspeed) {
  try {
    alerat(determineTyphoonIntensity(windspeed));
  } catch (error) {
    console.log(error.message);
  } finally {
    alert("Intensity updates will show new alert!");
  }
}
showIntensityAlert(56);
